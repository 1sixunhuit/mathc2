#include <Arduino.h>
#include <Servo.h>

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++            Définition pour préprocesseur                    ++ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

// ! pas de ; dans les #define car ils agissent lors du preprocess !

/* ***************************************************************** */
/* **           Définitions des PIN                               ** */
/* ***************************************************************** */
#define PinLEDL 8
#define PinLEDR 9
#define PinMOTL 12
#define PinMOTR 13
#define PinBUZZ 7

/* ***************************************************************** */
/* **           Autres définitions                                ** */
/* ***************************************************************** */


/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++            Définition des OBJETS                            ++ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
Servo MotL  ; 
Servo MotR  ;

// On peut faire la meme chose pour les variables et contantes globales
// D'apres ce que l'on m'a dit, il faut déclarer les servomoteurs avant
// les autres actionneurs s'il y en a, mais ce n'est pas  le cas ici
// (au-moins pour le moment)

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++            Définition des variables                         ++ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
long t(0);
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++            Fonctions de base                                ++ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

void setup(){ // Fonction principale d'initialisation

// Pour avoir un retour console
//Serial.begin(115200);
//Serial.println("Debug Robot");

/* ***************************************************************** */
/* **           Initialisations des PIN                           ** */
/* ***************************************************************** */
pinMode( PinLEDL , OUTPUT ) ;
pinMode( PinLEDR , OUTPUT ) ;

pinMode( PinMOTL , OUTPUT ) ;
pinMode( PinMOTR , OUTPUT ) ;
MotL.attach(    PinMOTL   ) ;
MotR.attach(    PinMOTR   ) ;

// On peut faire la meme chose pour les variables, structures, ...
// Cependant elles ne seront définies que pour cette fonction
// (sauf si elles sont globales et on leur assigne une valeur)

/* ***************************************************************** */
/* **    Envoyer une CONSIGNE aux ROUES                           ** */
/* ***************************************************************** */
MotR.writeMicroseconds(2000); // stop roue D
MotL.writeMicroseconds(2000); // stop roue G


digitalWrite(PinLEDL, HIGH) ;
digitalWrite(PinLEDR, HIGH) ;

delay(800)                  ; // Un peu moins d'une seconde avant que le robot ne démarre.
}




void loop(){ // Boucle qui tourne ad vitam eternam <=> while(1)
//90 deg environ 0.680s (696)
// 15.5 cm/min

/* ================================================================= */
/* ==              SCRIPT de déplacement                          == */
/* ================================================================= */

//Avancer de 30cm : 1850 secondes
MotR.writeMicroseconds(1900);
MotL.writeMicroseconds(2100);
delay(185000);

//Stop
MotR.writeMicroseconds(2000);
MotL.writeMicroseconds(2000);
delay(50000);

//Tourner de 90 degre dans le sens ... 
MotR.writeMicroseconds(2100);
MotL.writeMicroseconds(2100);
delay(696);


while(1){} // On le bouge qu'une fois
/* ================================================================= */
/* ==              FIN SCRIPT de déplacement                      == */
/* ================================================================= */
}

